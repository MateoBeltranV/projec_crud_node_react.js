import React, { Fragment, useState, useEffect } from 'react';
import Navbar from './Components/Navbar'
import Booklist from './Components/Booklist';
import Form from './Components/Form';

//para retornar varios elementos de un componente se usa fragment
function App() {

  const [book, setBook] = useState({
    titulo: '',
    autor: '',
    edicion: '0'
  })

//books es el estado de los libros en array, y setBooks es lo que me va permitir modificar dicho estado
  const [ books, setBooks ] = useState([])
// obtener los libros del banco de datos y los imprima automaticamente
  useEffect(() => {
    const getBooks = () => {
      fetch('http://localhost:9000/api') //ejecuta la Query
      .then(res => res.json())
      .then(res => setBooks(res))
    }
    getBooks()
  }, [])

  return (
   <Fragment>
     <Navbar brand='Library App'/>
     <div className='container'>
       <div className='row'>
         <div className='col-7'>
          <h2 style={{textAlign: 'center'}}>Book list</h2> 
          <Booklist books={books}/>
         </div>
         <div className='col-5'>
          <h2 style={{textAlign: 'center'}}>Book Form</h2>
          <Form book={book} setBook={setBook}/>
         </div>
       </div>
     </div>
   </Fragment>
  );
}

export default App;
