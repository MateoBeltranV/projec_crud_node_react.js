const express = require('express');
const mysql = require('mysql');
const myconn = require('express-myconnection');
const routes = require('./routes');
const cors = require('cors')
require('dotenv').config();

const app = express();

const { DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE } = process.env;

app.set('port', process.env.PORT || 9000)


const dbOptions = {
      host: DB_HOST,
      user: DB_USERNAME,
      password: DB_PASSWORD,
      port: 3306,
      database: DB_DATABASE
    };
  
//middlewares
app.use(myconn(mysql, dbOptions, 'single'));
app.use(express.json());
app.use(cors());

//rutes
app.use('/api', routes )

app.listen(app.get('port'), () => {
    console.log('server runnig on port', app.get('port'))
});